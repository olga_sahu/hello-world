const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cors = require("cors");

const CREDS = {
  LOGIN: "test",
  PASSWORD: "pass",
};

const PORT = 8000;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(
  cors({
    origin: "*",
    methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
    preflightContinue: false,
    optionsSuccessStatus: 204,
  })
);

app.post("/login", async function (req, res) {
  const { login, password } = req.body;
  console.log(login, password);
  if (!login || !password) {
    return res.status(401).send("No login or password, check your payload");
  }

  if (login === CREDS.LOGIN && password === CREDS.PASSWORD) {
    return res.status(200).send("Success");
  } else {
    return res.status(401).send("Invalid credentials");
  }
});

app.listen(PORT, function () {
  console.log(`Example app listening on port ${PORT}!`);
});
