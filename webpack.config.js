const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
  entry: path.join(__dirname, "app", "App.js"),
  output: {
    path: path.join(__dirname, "dist"),
    filename: "index.js",
    library: "lib"
  },

  plugins: [
         new HtmlWebpackPlugin({
         template: './app/template.html'
       })
     ],

  module: {
    rules: [
      { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" },

      {
        test: /\.svg$/,
        use: [
          {
            loader: "babel-loader"
          },
          {
            loader: "react-svg-loader",
            options: {
              jsx: true // true outputs JSX tags
            }
          }
        ]
      },

      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
      
      {
        test: /\.s[ac]ss$/i,
        use: [
          'style-loader',
          'css-loader',
          'sass-loader',
        ],
      },
    ]
  },

  devServer: {
    contentBase: path.join(__dirname, "dist"),
    compress: true
  }
};
