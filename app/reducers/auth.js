import {AUTH_REQUEST, AUTH_SUCCESS, AUTH_ERROR} from '../actions/auth';

const initialState = {
    isFetching: false,
    isAuthorized: false,
    error: null,
};

export const authReducer = (state = initialState, action) => {
    switch(action.type){
        case AUTH_REQUEST: 
        return {
            ...state,
            isFetching: true,
            error: null,
        };
        case AUTH_SUCCESS:
            return {
                ...state,
                isFetching: false,
                error: null,
                isAuthorized: true,
            };
        case AUTH_ERROR:
            return {
                ...state,
                isFetching: false,
                error: true,
                isAuthorized: false,
            };
        default:
            return state;
    }
};