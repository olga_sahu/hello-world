export default function todos(state = [], action) {
  switch (action.type) {
  case "ADD_TODO":
    return state.concat([action.payload]);
  case "TOGGLE_TODO":
    return state.map(item =>
      item.id === action.payload
        ? { ...item, complete: !item.complete }
        : item
    );
  case "DELETE_TODO":
    return state.filter(item => item.id !== action.payload);

    // case 'EDIT_TODO':
    //     return {
    //         ...state,
    //         todo: [...state.todo, action.payload]
    // };
  default:
    return state;
  }
}
