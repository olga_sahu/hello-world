import { combineReducers } from "redux";

import todos from "../reducers/todos";
import counter from "../reducers/counter";
import {authReducer} from "../reducers/auth";


export default combineReducers({
  todo: todos,
  counter,
  auth: authReducer,
});
