//Actions

export const addTodoAction = value => ({
  type: "ADD_TODO",
  payload: value
});

export const toggleTodoAction = id => ({
  type: "TOGGLE_TODO",
  payload: id
});

export const deleteTodoAction = id => ({
  type: "DELETE_TODO",
  payload: id
});

export const editTodoAction = id => ({
  type: "EDIT_TODO",
  payload: id
});
