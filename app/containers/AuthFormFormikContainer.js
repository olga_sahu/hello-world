import React from "react";
import { connect } from "react-redux";
import { login } from "../actions/auth";

import {AuthFormFormik} from "../components/AuthFormFormik/AuthFormFormik";

class AuthFormFormikContainer extends React.Component {
    handleLogin = (data) => {
      const {loginRequest} = this.props;
      console.log(data);
      loginRequest(data);
    };

    render(){
        return (<AuthFormFormik handleLogin={this.handleLogin} />);
    }
}

function mapStateToProps(state, ownProps){
    const auth = state.auth;
  
    return {
      isFetching: auth.isFetching,
      isAuthorized: auth.isAuthorized,
      error: auth.error,
      entries: auth.enties,
    };
  }
  
function mapDispathToProps(dispatch){
    return {
      loginRequest: (values) => dispatch(login(values)),
    }
  }
  
export default connect(mapStateToProps, mapDispathToProps)(AuthFormFormikContainer);