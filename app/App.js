import React from "react";
import ReactDOM from "react-dom";
import {apiMiddleware} from 'redux-api-middleware';
// import { Router, Route, hashHistory } from 'react-router';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Content from "./components/Content";
import Clock from "./components/Clock";
//import DateTime from "./components/DateTime/DateTime";
import DateTimeMultiLang from "./components/DateTimeMultiLang/DateTimeMultiLang";
//import DateTimeHooks from "./components/DateTimeHooks/DateTimeHooks";
import AuthForm from "./components/AuthForm/AuthForm";
import AuthFormFormikContainer from "./containers/AuthFormFormikContainer";
import "./styles/styles.scss";

//Redux
import { applyMiddleware, compose, createStore } from "redux";
import { Provider } from "react-redux";
import logger from "redux-logger";
import thunk from "redux-thunk";

import TodoList from "./components/Todo/TodoList";
import TodoInput from "./components/Todo/TodoInput";

// import uuid from 'uuid/v4';
import reducer from "./reducers/redux";

// const initialState = {
//   todo: [
//     {
//       id: uuid(),
//       name: 'Test 1',
//       complete: false
//     }
//   ],
// };

const store = createStore(reducer, {todo: [], counter: 0, auth: {}}, compose(
  applyMiddleware(logger, thunk, apiMiddleware),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
));

class App extends React.Component {
  render() {
    return (
      <div className="wrapper">
        <TodoList />
        <TodoInput />
        <hr />
        <AuthFormFormikContainer />
        {/* <AuthForm /> */}
        <Content />
        <Clock />
        {/*<DateTime />*/}
        <DateTimeMultiLang />
        {/*<DateTimeHooks />*/}
      </div>
    );
  }
}

let app = document.getElementById("app");

// ReactDOM.render(
//   <Provider store={store}>
//     <App />
//   </Provider>,
//   app
// );

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <Route path="/" component={App} />
    </Router>
  </Provider>,
  app
);
