import React from "react";
import { format } from "date-fns/esm";
import { ru, enUS, es } from "date-fns/esm/locale";
import "./DateTimeMultiLang.scss";
import classNames from "classnames";
import Select from "react-select";

const LOCALES = {
  RUSSIAN: "ru",
  ENGLISH: "en-US",
  SPANISH: "es"
};

const options = [
  { value: LOCALES.RUSSIAN, label: "Русский язык" },
  { value: LOCALES.ENGLISH, label: "Английский язык" },
  { value: LOCALES.SPANISH, label: "Испанский язык" }
];

export default class DateTimeMultiLang extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentTime: new Date(),
      locale: LOCALES.RUSSIAN,
    };

    this.setCurrentTime = this.setCurrentTime.bind(this);
    this.getDateWithLocale = this.getDateWithLocale.bind(this);
    this.getClassesForDate = this.getClassesForDate.bind(this);
  }

  componentDidMount = () => {
    this.timerID = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount = () => {
    clearInterval(this.timerID);
  }

  getDateWithLocale = (locale) => {
    const { currentTime } = this.state;
    return format(currentTime, "dd MMMM yyyy pp", { 'locale': this.getLocaleComponent(locale) });
  }

  getLocaleComponent = (localeCode) => {
    let component = ru;
    switch (localeCode) {
      case LOCALES.ENGLISH:
        component = enUS;
        break;
      case LOCALES.SPANISH:
        component = es;
        break;
      default: component = ru;
    }
    return component;
  }

  getClassesForDate = () => {
    const { locale } = this.state;

    return classNames({
      dateRu: locale === LOCALES.RUSSIAN,
      dateEn: locale === LOCALES.ENGLISH,
      dateEs: locale === LOCALES.SPANISH,
    });
  }

  changeLocale = (selectOption) => {
    this.setState({ locale: selectOption.value });
  };

  setCurrentTime = (date) => {
    this.setState({ currentTime: date });
  };

  tick = () => {
    this.setCurrentTime(new Date());
  };

  getCurrentLocaleValue(currentLocale) {
    return options.find(option => option.value === currentLocale);
  }

  render() {
    const { locale } = this.state;

    return (
      <div>
        <h3 className={this.getClassesForDate()}>
          {this.getDateWithLocale(locale)}
        </h3>
        <div className="parent-select-language">
          <Select
            options={options}
            value={this.getCurrentLocaleValue(locale)}
            onChange={this.changeLocale}
          />
        </div>
      </div>
    );
  }
}

