import React from "react";
import { connect } from "react-redux";
import { toggleTodoAction, deleteTodoAction } from "../../actions";
import "./Todo.scss";

const  TodoList = (props) => {
  const  { todo } = props;

  const toggleTodoAction = (todoId) => {
    props.toggleTodoAction(todoId);
  };

  const deleteTodoAction = (todoId) => {
    props.deleteTodoAction(todoId);
  };

  return (
    <div className="container">
      <h1>TODO List</h1>
      <ul>
        {todo.map((item, index) => (
          <li key={index}>
            <input
              type="checkbox"
              checked={item.complete}
              onChange={toggleTodoAction.bind(null, item.id)}
            />
            <span className={item.complete ? "complete" : null}>
              {item.name}
            </span>
            <hr />
            <button>Редактировать</button>
            <button onClick={deleteTodoAction.bind(null, item.id)}>Удалить</button>
          </li>
        ))}
      </ul>
    </div>
  );
};


const mapStateToProps = (state) => ({
  todo: state.todo
});

export default connect(mapStateToProps, { toggleTodoAction, deleteTodoAction })(TodoList);
