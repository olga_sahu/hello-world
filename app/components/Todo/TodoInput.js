import React, { useState } from "react";
import { connect } from "react-redux";
import {addTodoAction} from '../../actions';
import uuid from "uuid/v4";
//import { Divider } from "semantic-ui-react";

const TodoInput = (props) => {
  const [todo, setTodo] = useState('');


  const onSubmit = event => {
    event.preventDefault();
    props.addTodoAction({
      id: uuid(),
      name: todo,
      complete: false
    });
    setTodo('');
  };

  const onChange = event => {
    setTodo( event.target.value );
  };

  return (
    <div className="container">
      <form onSubmit={onSubmit}>
        <input
          type="text"
          name="todo"
          placeholder="Add task"
          value={todo}
          onChange={onChange}
        />
      </form>
    </div>
  );
};

const mapStateToProps = state => ({
  todo: state.todo
});

export default connect(mapStateToProps, { addTodoAction })(TodoInput);
