import React, { useState, useEffect } from "react";
import { format } from "date-fns/esm";
import { ru } from "date-fns/esm/locale";
import "./../../styles/styles.scss";
import "./DateTimeHooks.scss";
import classNames from "classnames";

const LOCALES = {
  RUSSIAN: 'rus',
  ENGLISH: 'eng'
}
export default function DateTimeHooks() {
  const [currentTime, setCurrentTime] = useState(new Date());
  const [locale, setLocale] = useState(LOCALES.RUSSIAN);

  let timerID;

  const isRussian = locale === LOCALES.RUSSIAN; 

  function changeLocale() {
    isRussian ? setLocale(LOCALES.ENGLISH) : setLocale(LOCALES.RUSSIAN);
  }

  useEffect(() => {
    if (!timerID) {  
      timerID = setInterval(() => tick(), 1000);
    }

    return function cleanup() {
      clearInterval(timerID);
    };
  });

  const tick = () => {
    setCurrentTime(new Date());
  };

  const dateClassName = classNames({ 
    dateRus: isRussian, 
    dateEngl: !isRussian 
  });

  const getDate = (date) => format(date, "dd MMMM yyyy pp", { locale: ru });

  const getDateEn = (date) => format(date, "dd MMMM yyyy pp");

  return (
    <div>
      <h3 className={dateClassName}>{isRussian ? getDate(currentTime) : getDateEn(currentTime)}</h3>
      <p>
        <button onClick={changeLocale}>Поменять формат даты</button>
      </p>
    </div>
  );
}
