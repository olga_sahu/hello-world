import React from "react";
import { Formik, Form, Field } from "formik";
import DatePicker, { setDefaultLocale } from "react-datepicker";
import { ru } from "date-fns/locale";
import * as Yup from "yup";

import "./AuthFormFormik.scss";

setDefaultLocale(ru);

const SignupSchema = Yup.object().shape({
  username: Yup.string()
    .min(2, 'Поле "Имя пользователя" должно содержать от 2 до 50 символов.')
    .max(50, 'Поле "Имя пользователя" должно содержать от 2 до 50 символов.!')
    .required("Обязательное поле"),
  password: Yup.string()
    .min(4, 'Поле "Пароль" должно содержать от 4 до 50 символов.')
    .max(50, 'Поле "Пароль" должно содержать от 4 до 50 символов.')
    .required("Обязательное поле")
});

export class AuthFormFormik extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      birthday: new Date()
    };
  }

  handleBirthdayChange = birthday => {
    this.setState({ birthday });
  };

  handleSendForm = (values) => {
    const {handleLogin} = this.props;
    if("username" in values){
      handleLogin(values);
    }
  };

  render() {
    const { birthday } = this.state;
    return (
      <div className="container">
        <section id="content">
          <Formik
            initialValues={{ username: "", password: "" }}
            validationSchema={SignupSchema}
            onSubmit={(values, { setSubmitting }) => {
              this.handleSendForm(values);
              // setTimeout(() => {
              //   console.log(JSON.stringify(values, null, 2));
              //   setSubmitting(false);
              // }, 400);
            }}
          >
            {({ errors, touched, isValidating, isSubmitting }) => (
              <Form>
                <h1>Login Form</h1>
                <Field
                  name="username"
                  type="text"
                  id="username"
                  placeholder="Имя пользователя"
                />
                {errors.username && touched.username ? (
                  <div>{errors.username}</div>
                ) : null}
                <div>
                  <Field
                    name="password"
                    type="password"
                    id="password"
                    placeholder="Пароль"
                  />
                  {errors.password && touched.password ? (
                    <div>{errors.password}</div>
                  ) : null}
                </div>
                <label className="AuthForm__birthday" htmlFor="birthday">
                  День рождения
                </label>
                <DatePicker
                  selected={birthday}
                  onChange={this.handleBirthdayChange}
                />

                <button type="submit" disabled={isSubmitting}>
                  Отправить
                </button>
              </Form>
            )}
          </Formik>
        </section>
      </div>
    );
  }
}