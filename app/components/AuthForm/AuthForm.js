import React from 'react';
import DatePicker, { registerLocale, setDefaultLocale } from 'react-datepicker';
import { ru } from 'date-fns/locale';
import './AuthForm.scss';
import 'react-datepicker/dist/react-datepicker.css';
import './react-datepicker-user.css';

setDefaultLocale (ru);

export default class AuthForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',
      birthday: new Date(),
      errors: [],
    };

    this.login = this.login.bind(this);
  }

  //Авторизация через форму на сайте
  //@param event - объект события (клик на кнопке)
  login(event) {
    //Отмена действия по умолчанию, чтобы страница не обновлялась
    event.preventDefault();

    const { username, password } = this.state;
    let errors = [];

    //Проверяем заполнение для username
    if (username.length < 2) {
      errors['username'] = 'Поле username должно содержать 2 и более символов.';
    }
    if (password.length < 4) {
      errors['password'] = 'Поле password должно содержать не менее 4-x символов.';
    }
    
    //Если ошибок нет, следовательно, массив с ошибками пустой
    if(!Object.keys(errors).length) {
      this.setState({username: '', password: '', errors: []});
    } else {
      this.setState({errors});
    }
  }

  handleChange = (value, name) => {
    this.setState({
      [name]:value
    }
    );
  };

  handleBirthdayChange = (birthday) => {
    this.setState({ birthday });
  };

  render() {
    const { username, password, birthday, errors } = this.state;
    return (
      <div className="AuthForm__container">
        <form className="transparent">
          <div className="form-inner">
            <h3>Регистрация</h3>
            <label htmlFor="username">Имя пользователя</label>
            <input value={ username } onChange={(event) => this.handleChange(event.target.value, event.target.id)} type="text" id="username" />
            <div className="AuthForm__error">{errors['username']}&nbsp;</div>
            <label htmlFor="password">Пароль</label>
            <input value={ password } onChange={(event) => this.handleChange(event.target.value, event.target.id)} type="password" id="password" />
            <div className="AuthForm__error">{errors['password']}&nbsp;</div>
            <label htmlFor="birthday">День рождения</label>
            <DatePicker 
            selected={birthday}
            onChange={this.handleBirthdayChange}
            />
            <input type="submit" value="Отправить" onClick={this.login} />
          </div>
        </form> 
        {JSON.stringify(this.state, null, 2)}
      </div>
    );
  }
}