import React from "react";
import { format } from "date-fns/esm";
import { ru } from "date-fns/esm/locale";
import "./DateTime.scss";
import classNames from "classnames";

const LOCALES = {
  RUSSIAN: "rus",
  ENGLISH: "eng"
};

export default class DateTime extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentTime: new Date(),
      locale: LOCALES.RUSSIAN
    };
    this.changeLocale = this.changeLocale.bind(this);
    this.setCurrentTime = this.setCurrentTime.bind(this);
  }

  componentDidMount() {
    this.timerID = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  getDate = (date) => format(date, "dd MMMM yyyy pp", { locale: ru });

  getDateEn = (date) => format(date, "dd MMMM yyyy pp");

  changeLocale() {
    this.getIsRussian()
      ? this.setState({ locale: LOCALES.ENGLISH })
      : this.setState({ locale: LOCALES.RUSSIAN });
  }

  getIsRussian = () => {
    return this.state.locale === LOCALES.RUSSIAN;  
  };

  setCurrentTime() {
    this.setState({ currentTime: new Date() });
  }

  tick = () => {
    this.setCurrentTime(new Date());
  };

  render() {
    const isRussian = this.getIsRussian();
    const {currentTime} = this.state;
    
    let btnClass = classNames({
      btn: true,
      dateRus: isRussian,
      dateEngl: !isRussian
    });

    return (
      <div>
        <h3 className={btnClass}>
           {isRussian
             ? this.getDate(currentTime)
             : this.getDateEn(currentTime)}
        </h3>
        <p>
          <button onClick={this.changeLocale}>Поменять формат даты</button>
        </p>
      </div>
    );
  }
}

