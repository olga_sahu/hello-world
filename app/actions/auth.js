import {createAction} from 'redux-api-middleware';

export const AUTH_REQUEST = 'AUTH_REQUEST';
export const AUTH_SUCCESS = 'AUTH_SUCCESS';
export const AUTH_ERROR = 'AUTH_ERROR';

export const login = (values) => createAction({
    endpoint: 'http://localhost:8000/login',
    method: 'POST',
    body: JSON.stringify({login: values.username, password: values.password}),
                headers: {
                    'Content-Type': 'application/json',
                },
    types: [
        AUTH_REQUEST,
        AUTH_SUCCESS,
        AUTH_ERROR,
    ]
});

// export const authLoginRequest = (login, password) => ({
//     type: AUTH_REQUEST,
//     payload: { login, password },
// });

// export const authLoginSuccess = (data) => ({
//     type: AUTH_SUCCESS,
//     payload: data,
// });

// export const authLoginError = (error) => ({
//     type: AUTH_ERROR,
//     payload: error,
// });

// export const login = (values) => {
//     const {username, password} = values;
//     console.log(username);
//     return async (dispatch) => {
//         try {
//             dispatch(authLoginRequest(username, password));
//             const result = (await fetch('http://localhost:8000/login', {
//                 body: JSON.stringify({username, password}),
//                 method: "POST",
//                 headers: {
//                     "Content-Type": "application/json",
//                 }
//             }));
//             dispatch(authLoginSuccess(await result.json()));
//         } catch(error){
//             console.log('Error()');
//             dispatch(authLoginError(error));
//         }
//     };
// };